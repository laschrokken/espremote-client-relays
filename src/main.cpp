#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include <ESP8266WiFiMulti.h>
ESP8266WiFiMulti WiFiMulti;
struct station_info *stat_info;
struct ip_addr *IPaddress;
IPAddress address;
const char* ssid = "ESP8266-Access-Point";
const char* password = "123456789";

//Your IP address or domain name with URL path
const char* serverNameIn = "http://192.168.4.1/in";
const char* serverNameOut = "http://192.168.4.1/out";


String in;
String out;
#define relay1 D1
#define relay2 D2
#define relay3 D5
#define relay4 D6
#define led 2


unsigned long previousMillis = 0;
unsigned long previousGetMillis = 0;

const long interval = 200; 
const long getInterval = 500; 

//WiFiMulti.setAutoReconnect(true);
//WiFiMulti.persistent(true);

String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;
  unsigned long currentGetMillis = millis();
//  if(currentMillis - previousMillis >= interval)     

  // Your IP address with path or Domain name with URL path 
  http.begin(client, serverName);
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "--"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
    //Serial.println(millis());

  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();
  Serial.println(currentGetMillis - previousGetMillis);
  previousGetMillis = currentGetMillis;

  return payload;
}

void setup() {
  Serial.begin(115200);
  Serial.println();
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  pinMode(relay3, OUTPUT);
  pinMode(relay4, OUTPUT);

  pinMode(led, OUTPUT);

  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Connected to WiFi");
}

void loop() {
  unsigned long currentMillis = millis();

  if(currentMillis - previousMillis >= interval) {
    Serial.println(millis());
     // Check WiFi connection status
    Serial.println(WiFiMulti.run());

    if ((WiFiMulti.run() == WL_CONNECTED)) {
      digitalWrite(led, LOW);

      in = httpGETRequest(serverNameIn);
      out = httpGETRequest(serverNameOut);
      Serial.println("in: " + in + " out: " + out );
      

      // save the last HTTP GET Request
      previousMillis = currentMillis;
    }else {
    
      Serial.println("WiFi Disconnected");
      digitalWrite(led, HIGH);

      WiFi.begin(ssid, password);
        while (WiFi.status() != WL_CONNECTED) {
          delay(100);
          Serial.print(".");
    
  
    }

    }
  }
  if(in=="0"){

    digitalWrite(relay1, HIGH);
    digitalWrite(relay2, HIGH);
    digitalWrite(relay3, HIGH);

  }else if(out=="0"){
    digitalWrite(relay1, HIGH);

  }
  else{
    digitalWrite(relay1, LOW);
    digitalWrite(relay2, LOW);
    digitalWrite(relay3, LOW);
    digitalWrite(relay4, LOW);

  }


}

